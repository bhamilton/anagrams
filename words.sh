#!/bin/bash
[[ -f bin ]] || javac src/com/gitlab/bhamilton/*.java -d bin
java -cp bin com.gitlab.bhamilton.WordsMain $@
