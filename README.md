Anagrams
=========

A very simple program for generating anagrams, so you can 
create crafty aliases and such.  This application evaluates its
solutions using parallel streams, so you won't run out of memory
but your CPU might get a bit hot.

## Usage

For generating word anagrams:

```
./words.sh red pandas
```

The script reads from `/usr/share/dict/words` which will
be present on any unix-based OS.

For generating name anagrams:

```
./names.sh bruce hamilton
```

The script reads from `first_names.txt` and `last_names.txt`.  
It will automatically first names for the first word and last names for the second word. 

These scripts will compile things automatically and execute the corresponding script.

### Optional Args

**-n**: Number of words to return, defaults to input word count.

```
./words.sh -n 3 Use this for exactly three words
```

**-l**: Min length of words, defaults to 2. 

```
./words.sh -l 4 I only want words with four or more characters
```
