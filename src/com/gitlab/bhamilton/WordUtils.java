package com.gitlab.bhamilton;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class WordUtils {

    static Stream<String> readWords(Path path, int minLength) throws IOException {
        return Files.lines(path).parallel()
                .filter(acceptWord(minLength))
                .map(WordUtils::cleanWord);
    }

    static Predicate<String> acceptWord(int minLength) {
        return str -> str.length() >= minLength && str.matches("[\\w ]+");
    }

    static String cleanWord(String str) {
        return str.replaceAll("\\W", "").toLowerCase();
    }

}
