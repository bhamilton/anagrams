package com.gitlab.bhamilton;

import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

public class WordForest implements WordSet {

    private final WordNode[] trees;

    public WordForest() {
        this(1);
    }
    public WordForest(int size) {
        trees = IntStream.range(0, size)
                .map(i -> 0)
                .mapToObj(WordNode::new)
                .toArray(WordNode[]::new);
    }

    @Override
    public void add(String word, int index) {
        trees[index].add(word);
    }

    @Override
    public void copy(int fromIndex, int toIndex) {
        trees[toIndex] = trees[fromIndex];
    }

    @Override
    public Stream<String> anagrams(String chars, int total) {
        return trees[0].anagrams(chars, "", 0, total);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("WordForest\n");
        for (int i=0; i < trees.length; i++)
            sb.append("\n\nTree ").append(i + 1).append(":\n").append(trees[i].toString());
        return sb.toString();
    }

    class WordNode {

        private final int depth;
        private final WordNode[] children;
        private boolean hit = false;

        public WordNode(int depth) {
            this.depth = depth;
            this.children = new WordNode[26];
        }

        public void add(CharSequence word) {
            if (depth == word.length()) {
                hit = true;
            } else {
                int c = word.charAt(depth) - 'a';
                if (children[c] == null)
                    children[c] = new WordNode(depth + 1);
                children[c].add(word);
            }
        }

        public Stream<String> anagrams(String chars,
                                       String match,
                                       int count,
                                       int total) {
            if (chars.length() == 1) {
                WordNode child = children[chars.charAt(0) - 'a'];
                return child != null && child.hit
                        ? Stream.of(match + chars)
                        : Stream.empty();
            }
            return IntStream.range(0, chars.length()).parallel()
                    .filter(i -> children[chars.charAt(i) - 'a'] != null)
                    .mapToObj(i -> {
                        WordNode child = children[chars.charAt(i) - 'a'];
                        String newMatch = match + chars.charAt(i);
                        String remainingChars = chars.substring(0, i) + chars.substring(i + 1);

                        // if this node forms a complete word, branch back to next root node
                        if (child.hit && count + 1 < total) {
                            return Stream.concat(
                                    child.anagrams(remainingChars, newMatch, count, total),
                                    trees[(count + 1) % trees.length].anagrams(remainingChars, newMatch + ' ', count + 1, total)
                            );
                        }
                        return child.anagrams(remainingChars, newMatch, count, total);
                    })
                    .flatMap(Function.identity());
        }

        @Override
        public String toString() {
            return toString("").limit(10).collect(joining("\n"));
        }

        private Stream<String> toString(String str) {
            return hit ? Stream.of(str) : IntStream.range(0, children.length)
                    .filter(i -> children[i] != null).boxed()
                    .flatMap(i -> children[i].toString(str + (char) (i + 'a')));
        }

    }

}
