package com.gitlab.bhamilton;

import java.util.stream.Stream;

public interface WordSet {

    void add(String word, int index);

    void copy(int fromIndex, int toIndex);

    default void add(String word) { add(word, 0); }

    Stream<String> anagrams(String chars, int n);

}
