package com.gitlab.bhamilton;

import java.util.OptionalInt;

public class AnagramConfig {

    static AnagramConfig fromArgs(String[] args) {
        int count = 0;
        OptionalInt wordCountArg = OptionalInt.empty();
        OptionalInt wordLengthArg = OptionalInt.empty();
        StringBuilder letters = new StringBuilder();
        for (int i=0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                char ch = args[i].charAt(1);
                switch (ch) {
                    case 'n' -> wordCountArg = OptionalInt.of(Integer.parseInt(args[i + 1]));
                    case 'l' -> wordLengthArg = OptionalInt.of(Integer.parseInt(args[i + 1]));
                }
                i++;
            } else {
                letters.append(WordUtils.cleanWord(args[i]));
                count++;
            }
        }
        return new AnagramConfig(
                wordCountArg.orElse(count),
                wordLengthArg.orElse(2),
                letters.toString()
        );
    }

    final int wordCount;
    final int minLength;
    final String letters;

    public AnagramConfig(int wordCount, int minLength, String letters) {
        this.wordCount = wordCount;
        this.minLength = minLength;
        this.letters = letters;
    }

}
