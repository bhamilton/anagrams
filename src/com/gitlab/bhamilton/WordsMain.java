package com.gitlab.bhamilton;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

public class WordsMain {

    private static final Path WORDS = Paths.get("/usr/share/dict/words");

    public static void main(String[] args) throws Exception {
        AnagramConfig conf = AnagramConfig.fromArgs(args);

        WordSet set = new WordForest();
        WordUtils.readWords(WORDS, conf.minLength).forEach(set::add);

        set.anagrams(conf.letters, conf.wordCount)
                .forEach(a -> System.out.append(a + '\n'));
        System.out.flush();
    }

}
