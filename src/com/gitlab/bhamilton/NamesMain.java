package com.gitlab.bhamilton;

import java.nio.file.Path;
import java.nio.file.Paths;

public class NamesMain {

    private static final Path FIRST_NAMES = Paths.get("first_names.txt"),
                              LAST_NAMES = Paths.get("last_names.txt");

    public static void main(String[] args) throws Exception {
        AnagramConfig conf = AnagramConfig.fromArgs(args);

        WordSet set = new WordForest(conf.wordCount);
        WordUtils.readWords(FIRST_NAMES, conf.minLength).forEach(w -> set.add(w, 0));
        for (int i=1; i < conf.wordCount - 1; i++)
            set.copy(0, i);
        WordUtils.readWords(LAST_NAMES, conf.minLength).forEach(w -> set.add(w, conf.wordCount - 1));

        set.anagrams(conf.letters, conf.wordCount)
                .forEach(a -> System.out.append(a + '\n'));
        System.out.flush();
    }

}
